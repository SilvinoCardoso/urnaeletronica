# Projeto urna eletrônica

## Exercício 02 

> Crie um tela de uma urna eletrônica. Esta tela deverá conter um campo para digitação do número eleitoral do candidato e um campo para apresentar o nome do candidato, a tela também deverá exibir a foto do candidato.
Caso o  eleitor não se recorde do número de seu candidato ele poderá  selecionar o número do candidato de uma lista.
A urna deverá contar com os botões "Confirmar", "Cancelar", "Branco"
