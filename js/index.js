var dados = []
var voto = []

window.onload = async function () {
    let listaCandidatos = await buscaCargaInicial()
    dados = listaCandidatos
    var opcoes = document.getElementById("idNumeroCandidato")

    if (listaCandidatos[0][0] == "a") {
        let divRg = document.getElementById("idDivRg")
        divRg.className = "invisible"
    }
    for (let index = 0; index < listaCandidatos.length; index++) {
        let opcao = document.createElement("option")
        opcao.innerHTML = listaCandidatos[index][2]
        opcao.value = listaCandidatos[index][2]
        opcoes.appendChild(opcao)
    }
}

async function buscaCargaInicial() {
    let response = await fetch(`http://localhost:3001/cargainicial`)
    return response.json()
}


var candidatoSelecionado = document.getElementById("idNumeroCandidato")

candidatoSelecionado.onchange = async function () {

    let nomeCandidato = document.getElementById("idNumeroCandidato").value
    let dadosCandidato = await buscaDadosCandidato(nomeCandidato)
    let nomeCandidatoTela = document.getElementById("idNomeCandidatoTela")
    nomeCandidatoTela.className = "fs-1 text-center"
    nomeCandidatoTela.innerText = dadosCandidato[2]

    let imgCandidato = document.getElementById("idFotoCandidato")
    imgCandidato.setAttribute("src", dadosCandidato[3])

    let textoNumeroCandidato = document.getElementById("idTextoNumeroCandidato")
    textoNumeroCandidato.className = "text-center fs-3"
    textoNumeroCandidato.innerHTML = "Número: " + dadosCandidato[1]

    voto = dadosCandidato
}

async function buscaDadosCandidato(candidato) {
    let response = await fetch(`http://localhost:3001/cargainicial/${candidato}`)
    return response.json()
}

var btnConfirmar = document.getElementById("idBtnConfirmar")

btnConfirmar.onclick = async function () {

    let votoConfirmado = {
        rgEleitor: document.getElementById("idNumeroRg").value,
        numeroCandidato: voto[1],
    }

    const options = {
        method: "POST",
        headers: {
            'Content-type': 'application/json',
        },
        body: JSON.stringify(votoConfirmado)
    }
    let response = await confirmaVoto(options)

    if (response.status == "200") {
        let mensagemOK = document.getElementById("idNomeCandidatoTela")
        let imgCandidato = document.getElementById("idFotoCandidato")
        imgCandidato.className = "invisible"
        mensagemOK.innerHTML = "Voto Registrado com Sucesso"
        mensagemOK.className = "text-success text-center"
        let textoNumeroCandidato = document.getElementById("idTextoNumeroCandidato")
        textoNumeroCandidato.innerHTML = ""

        setTimeout(() => {
            mensagemOK.innerHTML = "Governo Nacional"
            imgCandidato.className = "visible"
            imgCandidato.setAttribute("src", "./img/GOVBR.png")
            mensagemOK.className = "text-center mt-3"
        }, 2000);

        document.getElementById("idNumeroRg").value = ""
        document.getElementById("idNumeroCandidato").value = ""
    } else {
        let mensagemNok = document.getElementById("idNomeCandidatoTela")
        mensagemNok.innerHTML = "Erro ao registrar voto, contate o administrador do sistema"
        mensagemNok.className = "text-danger fs-6 text"

        let numeroRg = document.getElementById("idNumeroRg")
        numeroRg.setAttribute("disabled", "true")
        let inputCandidato = document.getElementById("idNumeroCandidato")
        inputCandidato.setAttribute("disabled", "true")

        let botoes = document.querySelectorAll(".btn")
        for (let index = 0; index < botoes.length; index++) {
            botoes[index].setAttribute("disabled", "true")
        }
    }
}

async function confirmaVoto(options) {
    let response = await fetch(`http://localhost:3001/voto`, options)
    return response.json()
}

var btnVotarBranco = document.getElementById("idBtnVotarBranco")

btnVotarBranco.onclick = async function () {

    let votoConfirmado = {
        rgEleitor: document.getElementById("idNumeroRg").value,
        numeroCandidato: "VotoBranco",
    }

    const options = {
        method: "POST",
        headers: {
            'Content-type': 'application/json',
        },
        body: JSON.stringify(votoConfirmado)
    }
    let response = await confirmaVoto(options)

    if (response.status == "200") {
        let mensagemOK = document.getElementById("idNomeCandidatoTela")
        let imgCandidato = document.getElementById("idFotoCandidato")
        imgCandidato.className = "invisible"
        mensagemOK.innerHTML = "Voto Registrado com Sucesso"
        mensagemOK.className = "text-success text-center"
        let textoNumeroCandidato = document.getElementById("idTextoNumeroCandidato")
        textoNumeroCandidato.innerHTML = ""

        setTimeout(() => {
            mensagemOK.innerHTML = "Governo Nacional"
            imgCandidato.className = "visible"
            imgCandidato.setAttribute("src", "./img/GOVBR.png")
            mensagemOK.className = "text-center mt-3"
        }, 2000);

        document.getElementById("idNumeroRg").value = ""
        document.getElementById("idNumeroCandidato").value = ""
    } else {
        let mensagemNok = document.getElementById("idNomeCandidatoTela")
        mensagemNok.innerHTML = "Erro ao registrar voto, contate o administrador do sistema"
        mensagemNok.className = "text-danger fs-6 text"

        let numeroRg = document.getElementById("idNumeroRg")
        numeroRg.setAttribute("disabled", "true")
        let inputCandidato = document.getElementById("idNumeroCandidato")
        inputCandidato.setAttribute("disabled", "true")

        let botoes = document.querySelectorAll(".btn")
        for (let index = 0; index < botoes.length; index++) {
            botoes[index].setAttribute("disabled", "true")
        }
    }
}

var btnAnularVoto = document.getElementById("idBtnAnularVoto")

btnAnularVoto.onclick = async function () {

    let votoConfirmado = {
        rgEleitor: document.getElementById("idNumeroRg").value,
        numeroCandidato: "VotoNulo",
    }

    const options = {
        method: "POST",
        headers: {
            'Content-type': 'application/json',
        },
        body: JSON.stringify(votoConfirmado)
    }
    let response = await confirmaVoto(options)

    if (response.status == "200") {
        let mensagemOK = document.getElementById("idNomeCandidatoTela")
        let imgCandidato = document.getElementById("idFotoCandidato")
        imgCandidato.className = "invisible"
        mensagemOK.innerHTML = "Voto Registrado com Sucesso"
        mensagemOK.className = "text-success text-center"
        let textoNumeroCandidato = document.getElementById("idTextoNumeroCandidato")
        textoNumeroCandidato.innerHTML = ""

        setTimeout(() => {
            mensagemOK.innerHTML = "Governo Nacional"
            imgCandidato.className = "visible"
            imgCandidato.setAttribute("src", "./img/GOVBR.png")
            mensagemOK.className = "text-center mt-3"
        }, 2000);

        document.getElementById("idNumeroRg").value = ""
        document.getElementById("idNumeroCandidato").value = ""
    } else {
        let mensagemNok = document.getElementById("idNomeCandidatoTela")
        mensagemNok.innerHTML = "Erro ao registrar voto, contate o administrador do sistema"
        mensagemNok.className = "text-danger fs-6 text"

        let numeroRg = document.getElementById("idNumeroRg")
        numeroRg.setAttribute("disabled", "true")
        let inputCandidato = document.getElementById("idNumeroCandidato")
        inputCandidato.setAttribute("disabled", "true")

        let botoes = document.querySelectorAll(".btn")
        for (let index = 0; index < botoes.length; index++) {
            botoes[index].setAttribute("disabled", "true")
        }
    }
}

var btnLimparTela = document.getElementById("idLimparTela")

btnLimparTela.onclick = function () {
    document.getElementById("idNumeroRg").value = ""
    document.getElementById("idNumeroCandidato").value = ""

    let mensagemOK = document.getElementById("idNomeCandidatoTela")
    let imgCandidato = document.getElementById("idFotoCandidato")
    imgCandidato.className = "invisible"
    mensagemOK.innerHTML = "Voto Registrado com Sucesso"
    mensagemOK.className = "text-success text-center"
    let textoNumeroCandidato = document.getElementById("idTextoNumeroCandidato")
    textoNumeroCandidato.innerHTML = ""

    mensagemOK.innerHTML = "Governo Nacional"
    imgCandidato.className = "visible"
    imgCandidato.setAttribute("src", "./img/GOVBR.png")
    mensagemOK.className = "text-center mt-3"
}