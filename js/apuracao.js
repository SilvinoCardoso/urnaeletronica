


window.onload = async function () {

    let resultado = await buscaResultado()

    
    for (let index = 0; index < resultado.length; index++) {
        var divResultCand = document.createElement("div")
        divResultCand.className = "card row d-flex flex-row p-3 mt-3"

        let colunaImgNome = document.createElement("div")
        colunaImgNome.className = "col-4 text-center"
        

        let imgCandidato = document.createElement("img")
        imgCandidato.setAttribute("src", resultado[index][2])
        imgCandidato.style.width = "100px"
        imgCandidato.style.height = "100px"


        let nomeCandidato = document.createElement("h2")
        nomeCandidato.className = "fs-4"
        
        if (resultado[index][0] == "VotoNulo") {
            console.log(resultado)
            nomeCandidato.innerText = " Votos Nulos"
        }if (resultado[index][0] == "VotoBranco") {
            nomeCandidato.innerText = " Votos Brancos"
        } else{
            nomeCandidato.innerText = resultado[index][1]
        }

        colunaImgNome.appendChild(imgCandidato)
        colunaImgNome.appendChild(nomeCandidato)

        let colunaNumeroCandidato = document.createElement("div")
        colunaNumeroCandidato.className = "col-5 "
        
        let numeroCandidato = document.createElement("h2")
        numeroCandidato.className = "fs-6"
        numeroCandidato.innerText = " Numero Candidato: " + resultado[index][0]
        
        colunaNumeroCandidato.appendChild(numeroCandidato)
        
        let linhaTotalVotosCandidato  = document.createElement("div")
        colunaNumeroCandidato.className = "col d-flex align-items-center justify-content-center flex-column"
        
        
        let totalVotosCandidato = document.createElement("h2")
        totalVotosCandidato.className = "fs-5"
        totalVotosCandidato.innerText = "Total de votos do candidato: " + resultado[index][3]
        linhaTotalVotosCandidato.appendChild(totalVotosCandidato)

        colunaNumeroCandidato.appendChild(linhaTotalVotosCandidato)
        
        
        divResultCand.appendChild(colunaImgNome)        
        divResultCand.appendChild(colunaNumeroCandidato)        
              
        let divResultado = document.getElementById("idDivResultado")
        divResultado.appendChild(divResultCand)
    }   
}


async function buscaResultado() {
    
    let result = await fetch("http://localhost:3001/apuracao")
    return result.json()
}